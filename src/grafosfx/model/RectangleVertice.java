package grafosfx.model;

import grafosfx.object.Vertice;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class RectangleVertice extends Vertice {
    public RectangleVertice(String id) {
        super(id);

        Rectangle view = new Rectangle(50,50);

        view.setStroke(Color.DODGERBLUE);
        view.setFill(Color.DODGERBLUE);

        StackPane pane = new StackPane();
        pane.getChildren().addAll(view, new Text(id));

        setView(pane);
    }

    public RectangleVertice(String id, Integer x, Integer y) {
        super(id, x, y);

        Rectangle view = new Rectangle(50,50);

        view.setStroke(Color.DODGERBLUE);
        view.setFill(Color.DODGERBLUE);

        StackPane pane = new StackPane();
        pane.getChildren().addAll(view, new Text(id));

        setView(pane);
    }
}
