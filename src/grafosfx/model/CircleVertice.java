package grafosfx.model;

import grafosfx.object.Vertice;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;

public class CircleVertice extends Vertice {
    public CircleVertice(String id) {
        super(id);

        Text text = new Text(id);
        text.setFont(Font.font("Verdana", FontPosture.ITALIC, 5));

        Circle view = new Circle(10);

        view.setStroke(Color.DODGERBLUE);
        view.setFill(Color.DODGERBLUE);

        StackPane pane = new StackPane();
        pane.getChildren().addAll(view, text);

        setView(pane);
    }

    public CircleVertice(String id, Integer x, Integer y) {
        super(id, x, y);

        Text text = new Text(id);
        text.setFont(Font.font("Verdana", FontPosture.ITALIC, 5));

        Circle view = new Circle(10);

        view.setStroke(Color.DODGERBLUE);
        view.setFill(Color.DODGERBLUE);

        StackPane pane = new StackPane();
        pane.getChildren().addAll(view, text);

        setView(pane);
    }
}
