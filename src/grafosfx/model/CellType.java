package grafosfx.model;

public enum CellType {
    RECTANGLE,
    TRIANGLE,
    CIRCLE
}