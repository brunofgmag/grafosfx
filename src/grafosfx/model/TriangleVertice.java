package grafosfx.model;

import grafosfx.object.Vertice;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class TriangleVertice  extends Vertice{
    public TriangleVertice(String id) {
        super( id);

        double width = 50;
        double height = 50;

        Polygon view = new Polygon( width / 2, 0, width, height, 0, height);

        view.setStroke(Color.RED);
        view.setFill(Color.RED);

        setView( view);
    }

    public TriangleVertice(String id, Integer x, Integer y) {
        super(id, x, y);

        double width = 50;
        double height = 50;

        Polygon view = new Polygon( width / 2, 0, width, height, 0, height);

        view.setStroke(Color.RED);
        view.setFill(Color.RED);

        setView( view);
    }
}
