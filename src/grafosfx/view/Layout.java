package grafosfx.view;

public abstract class Layout {
    public abstract void execute();
}
