package grafosfx.view;

import grafosfx.object.Graph;
import grafosfx.object.Vertice;

import java.util.List;
import java.util.Random;

public class RandomLayout extends Layout {
    private Graph graph;
    private Random rnd = new Random();

    public RandomLayout(Graph graph) {
        this.graph = graph;
    }

    public void execute() {
        List<Vertice> verticeList = graph.getModel().getAllVertices();

        for (Vertice vertice : verticeList) {
            double x;
            double y;

            if(vertice.getX() == null || vertice.getY() == null) {
                x = rnd.nextDouble() * 500;
                y = rnd.nextDouble() * 500;
            } else {
                x = vertice.getX();
                y = vertice.getY();
            }

            vertice.relocate(x, y);
        }
    }
}