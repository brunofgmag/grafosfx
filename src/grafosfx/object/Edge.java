package grafosfx.object;

import grafosfx.view.Arrow;
import javafx.scene.Group;
import javafx.scene.shape.Line;

public class Edge extends Group {
    private Vertice source;
    private Vertice target;
    private Line line;
    private Arrow arrow;

    public Edge(Vertice source, Vertice target) {

        this.source = source;
        this.target = target;

        source.addVerticeChild(target);
        target.addVerticeParent(source);

        source.addEdge(this);
        target.addEdge(this);

        line = new Line();
        //arrow = new Arrow();

        line.setStartX(source.getBoundsInParent().getWidth() / 2.0);
        line.setStartY(source.getBoundsInParent().getHeight() / 2.0);

        line.setEndX(target.getBoundsInParent().getWidth() / 2.0);
        line.setEndY(target.getBoundsInParent().getHeight() / 2.0);

        line.startXProperty().bind(source.layoutXProperty().add(source.getBoundsInParent().getWidth() / 2.0));
        line.startYProperty().bind(source.layoutYProperty().add(source.getBoundsInParent().getHeight() / 2.0));

        line.endXProperty().bind(target.layoutXProperty().add(target.getBoundsInParent().getWidth() / 2.0));
        line.endYProperty().bind(target.layoutYProperty().add(target.getBoundsInParent().getHeight() / 2.0));

        /*double angle = Math.atan2(arrow.getEndY() - arrow.getStartY(), arrow.getEndX() - arrow.getStartX()) * 180 / 3.14;

        double height = arrow.getEndY() - arrow.getStartY();
        double width = arrow.getEndX() - arrow.getStartX();
        double length = Math.sqrt(Math.pow(height, 2) + Math.pow(width, 2));

        double subtractWidth = 25 * width / length;
        double subtractHeight = 25 * height / length;

        setRotate(angle - 90);
        arrow.setTranslateX(arrow.getStartX());
        arrow.setTranslateY(arrow.getStartY());
        arrow.setTranslateX(line.getEndX() - subtractWidth);
        arrow.setTranslateY(line.getEndY() - subtractHeight);*/

        /*arrow.startXProperty().bind(source.layoutXProperty().add(source.getBoundsInParent().getWidth() / 2.0));
        arrow.startYProperty().bind(source.layoutYProperty().add(source.getBoundsInParent().getHeight() / 2.0));

        arrow.endXProperty().bind(target.layoutXProperty().add(target.getBoundsInParent().getWidth() / 2.0));
        arrow.endYProperty().bind(target.layoutYProperty().add(target.getBoundsInParent().getHeight() / 2.0));*/

        /*line.setTranslateX(line.getStartX());
        line.setTranslateX(line.getStartY());
        line.setTranslateX(line.getEndX()- target.getBoundsInParent().getWidth());
        line.setTranslateY(line.getEndY() - target.getBoundsInParent().getHeight());*/

        getChildren().add(line);
        //getChildren().add(arrow);
    }

    public Vertice getSource() {
        return source;
    }

    public Vertice getTarget() {
        return target;
    }

    public Line getLine() {
        return line;
    }
}
