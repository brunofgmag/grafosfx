package grafosfx.object;

import grafosfx.model.CellType;
import grafosfx.model.CircleVertice;
import grafosfx.model.RectangleVertice;
import grafosfx.model.TriangleVertice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {
    private Vertice graphParent;
    private Vertice startVertice;
    private Vertice stopVertice;

    private List<Vertice> allVertices;
    private List<Vertice> addedVertices;
    private List<Vertice> removedVertices;

    private List<Edge> allEdges;
    private List<Edge> addedEdges;
    private List<Edge> removedEdges;

    private Map<String,Vertice> verticesMap; // <id,vertice>

    Model() {
        graphParent = new Vertice( "_ROOT_");

        // clear model, create lists
        clear();
    }

    private void clear() {
        allVertices = new ArrayList<>();
        addedVertices = new ArrayList<>();
        removedVertices = new ArrayList<>();

        allEdges = new ArrayList<>();
        addedEdges = new ArrayList<>();
        removedEdges = new ArrayList<>();

        verticesMap = new HashMap<>(); // <id,cell>
    }

    public void clearAddedLists() {
        addedVertices.clear();
        addedEdges.clear();
    }

    public List<Vertice> getAddedVertices() {
        return addedVertices;
    }

    public List<Vertice> getRemovedVertices() {
        return removedVertices;
    }

    public List<Vertice> getAllVertices() {
        return allVertices;
    }

    public List<Edge> getAddedEdges() {
        return addedEdges;
    }

    public List<Edge> getRemovedEdges() {
        return removedEdges;
    }

    public List<Edge> getAllEdges() {
        return allEdges;
    }

    public Vertice getStartVertice() {
        return startVertice;
    }

    public Vertice getStopVertice() {
        return stopVertice;
    }

    public void setStartVertice(Vertice startVertice) {
        this.startVertice = startVertice;
    }

    public void setStopVertice(Vertice stopVertice) {
        this.stopVertice = stopVertice;
    }

    public Vertice getVertice(String id) {
        return verticesMap.get(id);
    }

    public void addVertice(String id, CellType type) {
        switch (type) {
            case RECTANGLE:
                RectangleVertice rectangleVertice = new RectangleVertice(id);
                addVertice(rectangleVertice);
                break;

            case TRIANGLE:
                TriangleVertice triangleVertice = new TriangleVertice(id);
                addVertice(triangleVertice);
                break;

            case CIRCLE:
                CircleVertice circleVertice = new CircleVertice(id);
                addVertice(circleVertice);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported type: " + type);
        }
    }

    public void addVertice(String id, Integer x, Integer y, CellType type) {
        switch (type) {
            case RECTANGLE:
                RectangleVertice rectangleVertice = new RectangleVertice(id, x, y);
                addVertice(rectangleVertice);
                break;

            case TRIANGLE:
                TriangleVertice triangleVertice = new TriangleVertice(id, x, y);
                addVertice(triangleVertice);
                break;

            case CIRCLE:
                CircleVertice circleVertice = new CircleVertice(id, x, y);
                addVertice(circleVertice);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported type: " + type);
        }
    }

    private void addVertice(Vertice vertice) {
        addedVertices.add(vertice);
        verticesMap.put(vertice.getVerticeID(), vertice);
    }

    public void addEdge( String sourceId, String targetId) {
        Vertice sourceVertice = verticesMap.get(sourceId);
        Vertice targetVertice = verticesMap.get(targetId);

        Edge edge = new Edge(sourceVertice, targetVertice);

        addedEdges.add(edge);
    }

    /**
     * Attach all cells which don't have a parent to graphParent
     * @param verticeList
     */
    public void attachOrphansToGraphParent(List<Vertice> verticeList) {
        for(Vertice vertice: verticeList) {
            if(vertice.getVerticeParents().size() == 0) {
                graphParent.addVerticeChild(vertice);
            }
        }

    }

    /**
     * Remove the graphParent reference if it is set
     * @param verticeList
     */
    public void disconnectFromGraphParent(List<Vertice> verticeList) {
        for(Vertice vertice: verticeList) {
            graphParent.removeVerticeChild(vertice);
        }
    }

    public void merge() {
        // cells
        allVertices.addAll(addedVertices);
        allVertices.removeAll(removedVertices);

        addedVertices.clear();
        removedEdges.clear();

        // edges
        allEdges.addAll(addedEdges);
        allEdges.removeAll(removedEdges);

        addedEdges.clear();
        removedEdges.clear();
    }
}
