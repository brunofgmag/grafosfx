package grafosfx.object;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class Vertice extends Pane {
    private String verticeID;
    private List<Vertice> children = new ArrayList<>();
    private List<Vertice> parents = new ArrayList<>();
    private List<Edge> edges = new ArrayList<>();
    private Integer x;
    private Integer y;
    private Node view;
    private boolean beenDiscovered;
    private boolean beenCompleted;
    private int depth;

    public Vertice(String verticeID) {
        this.verticeID = verticeID;
    }

    public Vertice(String verticeID, Integer x, Integer y) {
        this.verticeID = verticeID;
        this.x = x;
        this.y = y;
    }

    public void addVerticeChild(Vertice vertice) {
        children.add(vertice);
    }

    public List<Vertice> getVerticeChildrens() {
        return children;
    }

    public void addVerticeParent(Vertice vertice) {
        parents.add(vertice);
    }

    public List<Vertice> getVerticeParents() {
        return parents;
    }

    public void removeVerticeChild(Vertice vertice) {
        children.remove(vertice);
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setView(Node view) {
        this.view = view;
        getChildren().add(view);
    }

    public Node getView() {
        return this.view;
    }

    public String getVerticeID() {
        return verticeID;
    }

    public boolean hasBeenDiscovered() {
        return beenDiscovered;
    }

    public boolean hasBeenCompleted() {
        return beenCompleted;
    }

    public void setBeenDiscovered(boolean beenDiscovered) {
        this.beenDiscovered = beenDiscovered;
    }

    public void setBeenCompleted(boolean beenCompleted) {
        this.beenCompleted = beenCompleted;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
