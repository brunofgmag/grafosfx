package grafosfx.object;

import grafosfx.gesture.MouseGestures;
import grafosfx.view.VerticeLayer;
import grafosfx.view.ZoomableScrollPane;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Graph {
    private Model model;
    private Group canvas;
    private ZoomableScrollPane scrollPane;
    private MouseGestures mouseGestures;
    private VerticeLayer verticeLayer;

    public Graph() {
        this.model = new Model();

        canvas = new Group();
        verticeLayer = new VerticeLayer();

        canvas.getChildren().add(verticeLayer);

        mouseGestures = new MouseGestures(this);

        scrollPane = new ZoomableScrollPane(canvas);

        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);

    }

    public ScrollPane getScrollPane() {
        return this.scrollPane;
    }

    public Pane getVerticeLayer() {
        return this.verticeLayer;
    }

    public Model getModel() {
        return model;
    }

    public void beginUpdate() {
    }

    public void endUpdate() {
        Image img = new Image("file:maze.png");

        ImageView imgView = new ImageView(img);

        getVerticeLayer().getChildren().add(imgView);

        getVerticeLayer().getChildren().addAll(model.getAddedEdges());
        getVerticeLayer().getChildren().addAll(model.getAddedVertices());

        getVerticeLayer().getChildren().removeAll(model.getRemovedVertices());
        getVerticeLayer().getChildren().removeAll(model.getRemovedEdges());

        for (Vertice vertice : model.getAddedVertices()) {
            mouseGestures.makeDraggable(vertice);
        }

        // every cell must have a parent, if it doesn't, then the graphParent is
        // the parent
        getModel().attachOrphansToGraphParent(model.getAddedVertices());

        // remove reference to graphParent
        getModel().disconnectFromGraphParent(model.getRemovedVertices());

        // merge added & removed cells with all cells
        getModel().merge();

    }

    public double getScale() {
        return this.scrollPane.getScaleValue();
    }
}
