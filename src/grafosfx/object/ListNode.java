package grafosfx.object;

import java.util.ArrayList;
import java.util.List;

public class ListNode {
    private ListNode next;
    private Vertice vertice;

    private ListNode() {
    }

    public ListNode(Vertice rootVertice) {
        List<Vertice> incidentsVertices = new ArrayList<>();
        incidentsVertices.addAll(rootVertice.getVerticeChildrens());
        incidentsVertices.addAll(rootVertice.getVerticeParents());

        vertice = rootVertice;

        for(Vertice v : incidentsVertices) {
            if(next == null){
                ListNode node = new ListNode();
                node.setVertice(v);

                next = node;
            } else {
                ListNode n = next;

                while(n.getNext() != null){
                    n = n.getNext();
                }
                ListNode node = new ListNode();
                node.setVertice(v);

                n.setNext(node);
            }
        }
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public Vertice getVertice() {
        return vertice;
    }

    public void setVertice(Vertice vertice) {
        this.vertice = vertice;
    }
}
