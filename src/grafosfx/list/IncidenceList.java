package grafosfx.list;

import grafosfx.manager.ObjectManager;
import grafosfx.model.Label;
import grafosfx.object.Edge;
import grafosfx.object.Graph;
import grafosfx.object.ListNode;
import grafosfx.object.Vertice;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class IncidenceList {
    private static IncidenceList instancia;
    private Map<Vertice, ListNode> incidenceList = new HashMap<>();
    private ArrayDeque<Vertice> qList = new ArrayDeque<>();
    private boolean continueBFS;
    private Button continueButton;

    /*
    (o1, o2) -> {
        if (o1.getNomeVertice().compareTo(o2.getNomeVertice()) < 0) return -1;
        if (o1.getNomeVertice().compareTo(o2.getNomeVertice()) > 0) return 1;
        return 0;
    }
     */

    private IncidenceList(){}

    public static IncidenceList getInstance(){
        if(instancia == null) {
            instancia = new IncidenceList();
        }

        return instancia;
    }

    public void addVertice(Vertice v, ListNode n) {
        incidenceList.put(v, n);
    }

    public void startBFS(Graph graph){
        Vertice start = graph.getModel().getStartVertice();
        Vertice end = graph.getModel().getStopVertice();

        ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText("");

        continueButton.setOnAction(event -> {
            continueButton.setDisable(true);
            continueBFS = true;
        });

        if(start != null && end != null) {
            start.setBeenDiscovered(true);
            start.setDepth(0);
            ((Shape)((StackPane)start.getView()).getChildren().get(0)).setStroke(Color.BLACK);
            ((Shape)((StackPane)start.getView()).getChildren().get(0)).setFill(Color.BLACK);
            ((Text)((StackPane)start.getView()).getChildren().get(1)).setFill(Color.WHITE);
            ((Text)((StackPane)start.getView()).getChildren().get(1)).setText(start.getVerticeID() + "(0)");

            for(Edge edge : start.getEdges()) {
                edge.getLine().setFill(Color.RED);
                edge.getLine().setStroke(Color.RED);
            }

            List<Vertice> incidentsVertices = new ArrayList<>();
            incidentsVertices.addAll(start.getVerticeChildrens());
            incidentsVertices.addAll(start.getVerticeParents());

            for(Vertice vertice : incidentsVertices) {
                ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setStroke(Color.GRAY);
                ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setFill(Color.GRAY);
                ((Text)((StackPane)vertice.getView()).getChildren().get(1)).setFill(Color.WHITE);
                ((Text)((StackPane)vertice.getView()).getChildren().get(1)).setText(vertice.getVerticeID() + "(1)");
                vertice.setDepth(1);
                qList.addLast(vertice);

                if(vertice.equals(end)){
                    ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setStroke(Color.PURPLE);
                    ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setFill(Color.PURPLE);

                    ObjectManager.getInstance().getLabel(Label.LABEL_FINAL_DISTANCE).setText("1");
                    ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText("-");
                    return;
                }

                vertice.setBeenDiscovered(true);
            }

            for(Vertice v : qList) {
                ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText(
                        ObjectManager.getInstance().getLabel(Label.LABEL_Q).getText() + " " +
                                v.getVerticeID()
                );
            }

            continueButton.setDisable(false);

            (new Thread(() -> {
                while (true) {
                    try {
                        Thread.sleep(100);

                        if(continueBFS){
                            while(!qList.isEmpty()) {
                                if(continueBFS) {
                                    Platform.runLater(() -> {
                                        if(qList.isEmpty()) {
                                            return;
                                        }

                                        Vertice vertice = qList.poll();

                                        ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText("");

                                        for(Vertice ve : qList) {
                                            ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText(
                                                    ObjectManager.getInstance().getLabel(Label.LABEL_Q).getText() + " " +
                                                            ve.getVerticeID()
                                            );
                                        }

                                        ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setStroke(Color.BLACK);
                                        ((Shape)((StackPane)vertice.getView()).getChildren().get(0)).setFill(Color.BLACK);
                                        ((Text)((StackPane)vertice.getView()).getChildren().get(1)).setFill(Color.WHITE);
                                        if(vertice.getDepth() == 0) {
                                            ((Text)((StackPane)vertice.getView()).getChildren().get(1)).setText(vertice.getVerticeID() + "(" +
                                                    vertice.getDepth() + ")");
                                            vertice.setBeenCompleted(true);
                                        }

                                        for(Edge edge : vertice.getEdges()) {
                                            edge.getLine().setFill(Color.RED);
                                            edge.getLine().setStroke(Color.RED);
                                        }

                                        List<Vertice> incidents = new ArrayList<>();
                                        incidents.addAll(vertice.getVerticeChildrens());
                                        incidents.addAll(vertice.getVerticeParents());

                                        for(Vertice v : incidents) {
                                            if(v.hasBeenCompleted() && !vertice.hasBeenCompleted()) {
                                                vertice.setDepth(v.getDepth() + 1);
                                                vertice.setBeenCompleted(true);
                                            }

                                            if(!v.hasBeenDiscovered()){
                                                v.setDepth(vertice.getDepth() + 1);

                                                ((Shape)((StackPane)v.getView()).getChildren().get(0)).setStroke(Color.GRAY);
                                                ((Shape)((StackPane)v.getView()).getChildren().get(0)).setFill(Color.GRAY);
                                                ((Text)((StackPane)v.getView()).getChildren().get(1)).setFill(Color.WHITE);

                                                ((Text)((StackPane)v.getView()).getChildren().get(1)).setText(v.getVerticeID() + "(" +
                                                        v.getDepth() + ")");

                                                qList.addLast(v);

                                                ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText("");

                                                for(Vertice ve : qList) {
                                                    ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText(
                                                            ObjectManager.getInstance().getLabel(Label.LABEL_Q).getText() + " " +
                                                                    ve.getVerticeID()
                                                    );
                                                }

                                                if(v.equals(end)){
                                                    ObjectManager.getInstance().getLabel(Label.LABEL_FINAL_DISTANCE).setText(
                                                            Integer.toString(v.getDepth())
                                                    );
                                                    ((Shape)((StackPane)v.getView()).getChildren().get(0)).setStroke(Color.PURPLE);
                                                    ((Shape)((StackPane)v.getView()).getChildren().get(0)).setFill(Color.PURPLE);

                                                    v.setBeenDiscovered(true);
                                                    ObjectManager.getInstance().getLabel(Label.LABEL_Q).setText("-");
                                                    qList.clear();
                                                    continueButton.setDisable(true);
                                                    return;
                                                }

                                                v.setBeenDiscovered(true);
                                            }
                                        }
                                    });
                                    continueBFS = false;
                                    continueButton.setDisable(false);
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            })).start();
        }
    }

    private boolean allVerticesDiscovered(List<Vertice> incidents) {
        for(Vertice vertice : incidents) {
            if(!vertice.hasBeenDiscovered()) {
                return false;
            }
        }

        return true;
    }

    public void setContinueButton(Button continueButton) {
        this.continueButton = continueButton;
    }
}
