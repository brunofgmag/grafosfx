package grafosfx;

import grafosfx.list.IncidenceList;
import grafosfx.manager.ObjectManager;
import grafosfx.model.CellType;
import grafosfx.object.Graph;
import grafosfx.object.ListNode;
import grafosfx.object.Model;
import grafosfx.util.AlertUtils;
import grafosfx.view.Layout;
import grafosfx.view.RandomLayout;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main extends Application {
    Graph graph = new Graph();

    @Override
    public void start(Stage primaryStage) throws Exception{
        VBox vBox = new VBox();
        BorderPane root = new BorderPane();

        Button startButton = new Button("Iniciar BFS");
        VBox.setMargin(startButton, new Insets(5, 0, 5 , 5));

        Button continueButton = new Button("Continuar");
        continueButton.setDisable(true);
        VBox.setMargin(continueButton, new Insets(0, 0, 5 , 5));

        IncidenceList.getInstance().setContinueButton(continueButton);

        graph = new Graph();

        startButton.setOnAction((event) -> {
            if(graph.getModel().getStartVertice() == null || graph.getModel().getStopVertice() == null) {
                AlertUtils.buildAlert(Alert.AlertType.ERROR, StageStyle.DECORATED, "Você deve selecionar uma partida e um destino",
                        primaryStage, Modality.APPLICATION_MODAL).showAndWait();
            } else {
                startButton.setDisable(true);
                IncidenceList.getInstance().startBFS(graph);
            }
        });

        root.setCenter(graph.getScrollPane());

        Label topLabel = new Label("Selecione uma partida e um destino:");
        VBox.setMargin(topLabel, new Insets(0, 0, 5, 5));

        HBox qHBox = new HBox();

        Label qLabel = new Label("Q: ");
        HBox.setMargin(qLabel, new Insets(0, 0, 0, 5));

        Label qValueLabel = new Label("?");

        qHBox.getChildren().addAll(qLabel, qValueLabel);

        HBox valueHBox = new HBox();

        Label distanciaLabel = new Label("Menor distância (BFS): ");
        HBox.setMargin(distanciaLabel, new Insets(0, 0, 0, 5));

        Label distanciaValue = new Label("?");

        valueHBox.getChildren().addAll(distanciaLabel, distanciaValue);

        ObjectManager.getInstance().addLabel(grafosfx.model.Label.LABEL_Q, qValueLabel);
        ObjectManager.getInstance().addLabel(grafosfx.model.Label.LABEL_FINAL_DISTANCE, distanciaValue);

        vBox.getChildren().addAll(topLabel, root, qHBox, valueHBox, startButton, continueButton);

        Scene scene = new Scene(vBox, 800, 600);
        scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

        root.setPrefWidth(800);
        root.setPrefHeight(600);

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        primaryStage.setWidth(800);
        primaryStage.setHeight(600);
        primaryStage.setX((primaryScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
        primaryStage.setY((primaryScreenBounds.getHeight() - primaryStage.getHeight()) / 2);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Grafos");
        primaryStage.show();

        addGraphComponents();

        Layout layout = new RandomLayout(graph);
        layout.execute();
    }

    private void addGraphComponents() {
        Model model = graph.getModel();

        graph.beginUpdate();

        try(BufferedReader br = new BufferedReader(new FileReader("vertices.txt"))) {
            for(String line; (line = br.readLine()) != null; ) {
                String[] splitedLine = line.split(";");
                if(splitedLine.length <= 1) {
                    model.addVertice(line, CellType.CIRCLE);
                } else {
                    model.addVertice(splitedLine[0], Integer.valueOf(splitedLine[1]), Integer.valueOf(splitedLine[2]), CellType.CIRCLE);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try(BufferedReader br = new BufferedReader(new FileReader("edges.txt"))) {
            for(String line; (line = br.readLine()) != null; ) {
                String[] vertices = line.split(";");
                model.addEdge(vertices[0], vertices[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        graph.endUpdate();


    }
    public static void main(String[] args) {
        launch(args);
    }
}
