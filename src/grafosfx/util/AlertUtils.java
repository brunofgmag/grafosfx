package grafosfx.util;

import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.stage.*;

/**
 * Created by Bruno on 26/05/2017.
 * Brasil Linhas Aéreas
 */
public class AlertUtils
{
    private static Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

    /**
     * Constrói um Alert do JavaFX.
     * @param alertType tipo de Alert a ser mostrado.
     * @param style estilo visual do Alert.
     * @param content conteúdo do alerta.
     * @return objeto Alert.
     */
    public synchronized static Alert buildAlert(Alert.AlertType alertType, StageStyle style, String content){
        Alert alert = new Alert(alertType);
        alert.setTitle("Grafos");
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.initStyle(style);
        alert.setX((primaryScreenBounds.getWidth() - 425) / 2);
        alert.setY((primaryScreenBounds.getHeight() - 130) / 2);

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(ClassLoader.getSystemResource("default.css").toExternalForm());

        return alert;
    }

    /**
     * Constrói um Alert do JavaFX.
     * @param alertType tipo de Alert a ser mostrado.
     * @param style estilo visual do Alert.
     * @param owner dono do Alert.
     * @param modality tipo de modal do Alert.
     * @param content conteúdo do alerta.
     * @return objeto Alert.
     */
    public synchronized static Alert buildAlert(Alert.AlertType alertType, StageStyle style, String content, Window owner, Modality modality){
        Alert alert = new Alert(alertType);
        alert.setTitle("Grafos");
        alert.initOwner(owner);
        alert.initModality(modality);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.initStyle(style);
        alert.setX((primaryScreenBounds.getWidth() - 425) / 2);
        alert.setY((primaryScreenBounds.getHeight() - 130) / 2);

        return alert;
    }

    /**
     * Constrói um Alert do JavaFX.
     * @param alertType tipo de Alert a ser mostrado.
     * @param style estilo visual do Alert.
     * @param owner dono do Alert.
     * @param modality tipo de modal do Alert.
     * @param header header a ser mostrado no Alert.
     * @param content conteúdo do Alert.
     * @return objeto Alerta
     */
    public synchronized static Alert buildAlert(Alert.AlertType alertType, StageStyle style, String header, String content, Window owner, Modality modality){
        Alert alert = new Alert(alertType);
        alert.setTitle("Grafos");
        alert.initOwner(owner);
        alert.initModality(modality);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initStyle(style);
        alert.setX((primaryScreenBounds.getWidth() - 425) / 2);
        alert.setY((primaryScreenBounds.getHeight() - 130) / 2);

        return alert;
    }

    /**
     * Constrói um Alert do JavaFX.
     * @param alertType tipo de Alert a ser mostrado.
     * @param style estilo visual do Alert.
     * @param header header a ser mostrado no Alert.
     * @param content conteúdo do Alert.
     * @return objeto Alerta
     */
    public synchronized static Alert buildAlert(Alert.AlertType alertType, StageStyle style, String header, String content){
        Alert alert = new Alert(alertType);
        alert.setTitle("Grafos");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.initStyle(style);
        alert.setX((primaryScreenBounds.getWidth() - 425) / 2);
        alert.setY((primaryScreenBounds.getHeight() - 130) / 2);

        return alert;
    }
}
