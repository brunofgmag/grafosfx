package grafosfx.gesture;

import grafosfx.object.Graph;
import grafosfx.object.Vertice;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public class MouseGestures {
    private final DragContext dragContext = new DragContext();
    private Graph graph;
    private boolean isDragging;

    public MouseGestures(Graph graph) {
        this.graph = graph;
    }

    public void makeDraggable(final Node node) {
        node.setOnMousePressed(onMousePressedEventHandler);
        node.setOnMouseDragged(onMouseDraggedEventHandler);
        node.setOnMouseReleased(onMouseReleasedEventHandler);
    }

    private EventHandler<MouseEvent> onMousePressedEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            Node node = (Node) event.getSource();
            double scale = graph.getScale();

            dragContext.x = node.getBoundsInParent().getMinX() * scale - event.getScreenX();
            dragContext.y = node.getBoundsInParent().getMinY()  * scale - event.getScreenY();

        }
    };

    private EventHandler<MouseEvent> onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            Node node = (Node) event.getSource();

            double offsetX = event.getScreenX() + dragContext.x;
            double offsetY = event.getScreenY() + dragContext.y;

            // adjust the offset in case we are zoomed
            double scale = graph.getScale();

            offsetX /= scale;
            offsetY /= scale;

            node.relocate(offsetX, offsetY);

            isDragging = true;
        }
    };

    private EventHandler<MouseEvent> onMouseReleasedEventHandler = event -> {
        if(!isDragging){
            Node node = (Node) event.getSource();

            if(graph.getModel().getStartVertice() == null) {
                graph.getModel().setStartVertice(graph.getModel().getVertice(((Vertice) node).getVerticeID()));
                ((Shape)((StackPane)((Vertice) node).getView()).getChildren().get(0)).setStroke(Color.RED);
            } else if(graph.getModel().getStopVertice() == null) {
                graph.getModel().setStopVertice(graph.getModel().getVertice(((Vertice) node).getVerticeID()));
                ((Shape)((StackPane)((Vertice) node).getView()).getChildren().get(0)).setStroke(Color.PURPLE);
            }
        }

        isDragging = false;
    };

    class DragContext {
        double x;
        double y;
    }
}
