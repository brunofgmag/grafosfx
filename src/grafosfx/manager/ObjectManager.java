package grafosfx.manager;

import grafosfx.model.Label;

import java.util.EnumMap;

public class ObjectManager {
    private static ObjectManager instance;
    private final EnumMap<Label, javafx.scene.control.Label> jLabelEnumMap = new EnumMap<>(Label.class);

    private ObjectManager() { }

    public static ObjectManager getInstance() {
        if (instance == null) {
            instance = new ObjectManager();
        }
        return instance;
    }

    public void addLabel(Label ID, javafx.scene.control.Label label){
        jLabelEnumMap.put(ID, label);
    }

    public void cleanManager() {
        jLabelEnumMap.clear();
        System.out.println("Object manager cleared.");
    }

    public javafx.scene.control.Label getLabel(Label ID){
        return jLabelEnumMap.get(ID);
    }


    public EnumMap getLabelsMap(){
        return jLabelEnumMap;
    }
}